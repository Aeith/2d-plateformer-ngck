#pragma once
#include "Ngck_moteur/Ngck.h"
#include "Environment.h"
#include "Player.h"
#include "Enemy.h"
#include "Item.h"

#include <SFML/Graphics/Image.hpp>
#include <iostream>
#include <vector>

// Load a level from an image
class LoadLevelFromImage {
private:
	// Forbidden
	LoadLevelFromImage() = delete;
	~LoadLevelFromImage() = delete;
public :
	static bool sLoadFromImage(sf::Image& pImage, Player*& playR, std::vector< std::vector<GameObject*>>& allEntities);
};