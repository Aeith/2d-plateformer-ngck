#include "Item.h"

// Constructors / Desctructor
Item::Item() : GameObject(), defense(0.0f) {}
Item::Item(bool a, char* n, int s, float m, State st, f2d* spe, f2d* sz, f2d* pos, f2d* t, f2d* dir) : GameObject(a, n, s, m, st, spe, sz, pos, t, dir), defense(0.0f) {}
Item::Item(bool a, char* n, int s, float m, State st, f2d* spe, f2d* sz, f2d* pos, f2d* t, f2d* dir, float d, Type ty) : GameObject(a, n, s, m, st, spe, sz, pos, t, dir), defense(d), type(ty) {}
Item::~Item() {}

// Methods
void Item::Move(f2d* dir, std::vector<std::vector<GameObject*>> allEntities) {
	// Move if x dir
	if (state != FALLING) {
		if (dir->x != 0) {
			state = PUSHING;
			GameObject::Move(new f2d((dir->x*speed->x), 0), allEntities);
			state = WAITING;
		}
	}
}