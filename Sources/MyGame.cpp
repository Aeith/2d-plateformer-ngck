#include "Ngck_moteur/Ngck.h"
#include "GameObject.h"
#include "Player.h"
#include "Item.h"
#include "Enemy.h"
#include "Environment.h"
#include <iostream>
#include <list>
#include <vector>
#include <string>
// PLATEFORMER
// Aventurier fou, vous avez r�cup�rer une boule �nerg�tique dans un trou noir et depuis, les 2 races majeures de la Galaxie souhaite vous exterminer avant que vous ne l'utilisiez pour changer votre ampoule
// Bas� sur les masses et la gravit�
// Niveaux
// 3 diff�rents items propulseurs : Light, Medium, Heavy
	// Light : L�ger et maniable, elle n'apporte aucune d�fense
	// Medium : �quilibr�, il octroie une d�fense standard et une course normale
	// Heavy : Lourde, elle permet cependant de d�placer des objets lourds et rends invincible
// Gameplay
	// Caisse bois : � faire tomber pour tuer un ennemi ou r�cup�rer un objet
	// Caisse en fer : � d�placer
	// Boule �nerg�tique : � transporter jusqu'� la fin du jeu
	// Ennemi de pourchasse (� fuir)
	// Ennemi d'attente (� tuer)

// PHASES
	// INTRO : Intro r�captiulant votre vol et �chapp�e
	// TUTORIEL : Niveau tutoriel permettant de se familiariser avec tout les �l�ments de gameplay un � un
	// NIIVEAU X : Niveau de jeu
	// WIN : Affichage de la fin de niveau
	// END : Affichage de la fin du jeu


// Logique des ennemis :
	// Chaser :	-150 pixels : poursuit (80% vitesse du joueur,  80% masse) (Coup de dague)
	// Guard : One shot si - 50 pixels (Charge + Coup d'�p�e)

// PROTOTYPES
void GameUpdateIntroduction(float deltatime);
void GameUpdateLevel(float deltatime);
void GameUpdateEnd(float deltatime);
void GameDisplayIntroduction();
void GameDisplayLevel();
void GameDisplayEnd();

// WINDOW SIZES AND LEVEL MAX SIZE
#define WINDOW_HEIGHT	540
#define WINDOW_WIDTH	960

// Enum
enum PHASES {
	INTRO,
	LEVEL,
	WIN,
	END
};

// Global
PHASES phases;
std::vector<std::vector<GameObject*>> allEntities;
std::vector<GameObject*> allEnvironnement;
std::vector<GameObject*> allEnemies;
std::vector<GameObject*> allItems;
Player* player = new Player();
int sky;

// -----------------------------------------------------------
// -----------------------------------------------------------
void GameInitialise()
{
	// Start
	phases = INTRO;
	sky = KLoadSprite("largesky.png");
	allEntities.push_back(allEnvironnement);
	allEntities.push_back(allEnemies);
	allEntities.push_back(allItems);
}


// -----------------------------------------------------------
// -----------------------------------------------------------
// deltatime, in seconds
void GameUpdate(float deltatime)
{
	switch (phases) {
		case INTRO:
			GameUpdateIntroduction(deltatime);
			break;
		case LEVEL:
			GameUpdateLevel(deltatime);
			break;
		case END:
			break;
			GameUpdateEnd(deltatime);
		default:
			break;
	}
}

void GameUpdateIntroduction(float deltatime) {
	/// TODO INTRODUCTION

	// LOAD FIRST LEVEL
	Level::Init();
	if (!Level::Load(player, allEntities)) {
		std::cout << "Level 0 error" << std::endl;
		phases = END;
	}

	phases = LEVEL;
}

void GameUpdateLevel(float deltatime) {
	// LEVEL ENDED
	if (Level::isFinished) {
		Level::Reset(player);
		Level::Reset(allEntities);
		Level::Clear(allEntities);
		// Generate next path
		Level::Next(); // Auto generate Level::Init()
		// Can't load next level => end of the levels
		if (!Level::Load(player, allEntities)) {
			phases = END;
		}
	}
	else {
		// GRAVITY
		for each (std::vector<GameObject*> list in allEntities) {
			for each (GameObject* object in list) {
				// Check if f2d transform and direction are set
				if (object->isActive && object->transform && object->direction) 
				{
					// Check if the object is not Environnement or Enemy (no gravity applied)
					if (dynamic_cast<Environment*>(object) == nullptr && dynamic_cast<Enemy*>(object) == nullptr) {
						object->ApplyGravity(deltatime, allEntities);
					}
					else {
						if (dynamic_cast<Enemy*>(object) != nullptr) {
							// Check distance
							dynamic_cast<Enemy*>(object)->checkDistance(player, allEntities);
						}

						// Decrement deltatime to the message display timer
						if (dynamic_cast<Environment*>(object) != nullptr && dynamic_cast<Environment*>(object)->isExit && dynamic_cast<Environment*>(object)->message > 0) {
							dynamic_cast<Environment*>(object)->message -= deltatime;
						}
					}
				}
			}
		}
		player->ApplyGravity(deltatime, allEntities);

		// MOVE
		if (KGetLeft()) {
			player->Move(new f2d(-player->speed->x, 0), allEntities);
		}
		if (KGetRight()) {
			player->Move(new f2d(player->speed->x, 0), allEntities);
		}
		if ((KGetUp() || KGetSpace()) && (player->state != JUMPING && player->state != FALLING)) {
			player->state = JUMPING;
			player->Move(new f2d(0, -player->speed->y), allEntities);
		}
	}
}

void GameUpdateEnd(float deltatime) {
	/// TODO END
}


// -----------------------------------------------------------
// -----------------------------------------------------------
void GameDisplay()
{
	// Call phases
	switch (phases) {
		case INTRO:
			GameDisplayIntroduction();
			break;
		case LEVEL:
			GameDisplayLevel();
			break;
		case END:
			GameDisplayEnd();
			break;
		default:
			break;
	}
}

void GameDisplayIntroduction() {}

void GameDisplayLevel() {
	// Display sky BACK
	KPrintSprite(sky, -50, 0, 1, 1);

	// Display all entities - Back to Front : Environment - Enemies - Items
	for each (std::vector<GameObject*> list in allEntities) {
		for each (GameObject* object in list) {
			// Check if f2d transform and direction are set
			if (object->isActive && object->transform && object->direction) {
				// Display in offset range (0 + offset*20 && SCREENSIZE + offset*20)
				if ((object->transform->x < (SCREENSIZEX + (Level::offset->x * 20))) && (object->transform->y < (SCREENSIZEY + (Level::offset->x * 20)))) {
					// Call object display
					object->Display();
					continue;
				}
			}
		}
	}

	// Display player FRONT
	player->Display();
}

void GameDisplayEnd() {}
