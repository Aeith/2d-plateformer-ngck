#include "GameObject.h"

#define MAX_FALLING_HEIGHT	9

// Constructors / Desctructor
GameObject::GameObject() : isActive(true), sprite(0), mass(0), state(WAITING), speed(new f2d()), size(new f2d()), position(new f2d()), transform(new f2d()), direction(new f2d()) {}
GameObject::GameObject(bool a, char* n, int spr, float m, State st, f2d* spe, f2d* siz, f2d* pos, f2d* t, f2d* dir) : isActive(a), name(n), sprite(spr), mass(m), speed(spe), state(st), size(siz), position(pos), transform(t), direction(dir) {}
GameObject::~GameObject() {
	delete speed;
	speed = nullptr;
	
	delete size;
	size = nullptr;

	delete position;
	position = nullptr;

	delete transform;
	transform = nullptr;

	delete direction;
	direction = nullptr;
}

// Methods
GameObject& GameObject::operator=(const GameObject& object) {
	// A=A erroraddsa
	if (this != &object) {
		sprite = object.sprite;
		delete speed; speed = nullptr;
		if (object.speed) {
			speed = object.speed;
		}
		delete size; size = nullptr;
		if (object.size) {
			size = object.size;
		}
		delete position; position = nullptr;
		if (object.position) {
			position = object.position;
		}
		delete transform; transform = nullptr;
		if (object.transform) {
			transform = object.transform;
		}
		delete direction; direction = nullptr;
		if (object.direction) {
			direction = object.direction;
		}
	}
	return *this;
}

void GameObject::addSprite(char* path) {
	// Check existence
	if (path) {
		// Set sprite path
		this->sprite = KLoadSprite(path);
	}
}

void GameObject::ApplyGravity(float deltatime, std::vector<std::vector<GameObject*>> allEntities) {
	// Active object
	if (this->isActive) {
		// Apply gravity if nothing is 1pixel below
		float acc = deltatime * ACCELERATION;
		f2d distance = WillCollide(new f2d(0, 1), allEntities);
		if (distance == f2d()) {
			state = FALLING;
			direction->y += acc;
		}

		state = WAITING;

		// Move entity
		SetPosition(new f2d(0, direction->y), allEntities);
	}
}

void GameObject::Move(f2d* dir, std::vector<std::vector<GameObject*>> allEntities) {
	// Move entity
	SetPosition(dir, allEntities);
}

void GameObject::SetPosition(f2d* dir, std::vector<std::vector<GameObject*>> allEntities) {
	// Get distance with closest gonna be obsctacle (nullptr if nothing within range)
	f2d distance = WillCollide(dir, allEntities);

	// No collision
	if (distance == f2d()) {
		// Above level
		if (transform->y + dir->y <= (Level::size->y * 20)) {
			// Move entity
			transform->x += (int)dir->x;
			transform->y += (int)dir->y;
		}
		// Below level
		else {
			// Player fell => reset
			if (dynamic_cast<Player*>(this)) {
				Level::Reset(this);
				Level::Reset(allEntities);
			}

			// Reload entity in 150Y and reset Y direction (clear ACCELERATION)
			*transform = *position;
			direction->y = 0;
		}
	}
	else {
		// Reset acceleration and transform on top of the object
		if (dir->y != 0) {
			if (dir->y > 0) {
				direction->y = 0;
			}

			state = WAITING;
		}

		transform->x += (int) distance.x;
		transform->y += (int) distance.y;
	}
}

f2d GameObject::WillCollide(f2d* dir, std::vector<std::vector<GameObject*>> allEntities) {
	// Check for each entity
	if (*dir != f2d(0, 0)) {
		for each (std::vector<GameObject*> list in allEntities) {
			for each (GameObject* object in list) {
				// Active object
				if (object->isActive) {
					// A = A
					if (object != this) {
						f2d distance;

						// Going right or left
						if (dir->x != 0) {
							// Check enemies and env
							if (!(dynamic_cast<Environment*>(object) != nullptr && dynamic_cast<Environment*>(object)->isPedestral)) {
								// Check is transform is set
								if (object->transform) {
									// Object between Y and Y + Size.Y => height
									if (((transform->y + size->y) - object->transform->y) > 0 ^ (transform->y - (object->transform->y + object->size->y)) > 0) {
										// Right
										if (dir->x > 0) {
											// CHECK IF NEXT TRANSFORM WILL OVERLAP OBJECT
											// Object between X and X + Size.Y => in direction
											if ((transform->x + size->x <= object->transform->x) && (object->transform->x < (transform->x + size->x + dir->x))) {
												// Distance = A.dist(B);
												distance.x = object->transform->x - (transform->x + size->x);
												if (distance.x == 0) {
													distance.x = 0.1;
												}
											}
										}
										// Left
										else {
											// CHECK IF NEXT TRANSFORM WILL OVERLAP OBJECT
											// if object is between this and this+dir => in the direction
											if ((transform->x >= object->transform->x + object->size->x) && (object->transform->x + object->size->x > (transform->x + dir->x))) {
												distance.x = transform->x - (object->transform->x + object->size->x);
												if (distance.x == 0) {
													distance.x = -0.1;
												}
											}
										}
									}
								}
							}
						}
						// Going down or up
						if (dir->y != 0) {
							// Check enemies and env
							if (!(dynamic_cast<Environment*>(object) != nullptr && dynamic_cast<Environment*>(object)->isPedestral)) {
								// Check is transform is set
								if (object->transform) {
									// Object between X and X + Size.X => width
									if (((transform->x + size->x) - object->transform->x) > 0 ^ (transform->x - (object->transform->x + object->size->x)) > 0) {
										// Going down
										if (dir->y > 0) {
											// CHECK IF NEXT TRANSFORM WILL OVERLAP OBJECT
											// if object is between this and this+dir => in the direction
											if ((transform->y + size->y <= object->transform->y) && (object->transform->y < (transform->y + size->y + dir->y))) {
												distance.y = object->transform->y - (transform->y + size->y);
												if (distance.y == 0) {
													distance.y = 0.1;
													return distance;
												}
											}
										}
										// Going up
										if (dir->y < 0) {
											// CHECK IF NEXT TRANSFORM WILL OVERLAP OBJECT
											// if object is between this and this+dir => in the direction
											if ((transform->y >= (object->transform->y + object->size->y)) && ((object->transform->y + object->size->y) > (transform->y + dir->y))) {
												distance.y = transform->y - (object->transform->y + object->size->y);
												if (distance.y == 0) {
													distance.y = -0.1;
												}
											}
										}
									}
								}
							}
						}

						// Player
						if (dynamic_cast<Player*>(this) != nullptr) {
							// Move Ball as the player move
							if (dynamic_cast<Item*>(object) != nullptr && dynamic_cast<Item*>(object)->type == ENERGYBALL) {
								Player* p = dynamic_cast<Player*>(this);
								// Ball is taken
								if (p->taken) {
									// Set ball transform where player is
									dynamic_cast<Item*>(object)->transform->x = p->transform->x + (p->size->x / 2);
									dynamic_cast<Item*>(object)->transform->y = p->transform->y + (p->size->y / 2);
								}
							}
						}

						// Box
						if (dynamic_cast<Item*>(this) != nullptr && dynamic_cast<Item*>(this)->type == BOX) {
							Item* i = dynamic_cast<Item*>(this);
							// Fall from far enough => destroy && kill
							if (i->direction->y > MAX_FALLING_HEIGHT && distance != f2d()) {
								// Enemy hit
								if (dynamic_cast<Enemy*>(object)) {
									Enemy* e = dynamic_cast<Enemy*>(object);
									// Chaser => killed
									if (e->name == "chaser") {
										// Destroy enemy
										e->isActive = false;
									}
									// Guard => killed if steel
									if (e->name == "guard") {
										if (i->name == "Steelbox") {
											// Destroy enemy
											e->isActive = false;
										}
									}
								}

								// Destroy box
								i->isActive = false;
							}
									
						}

						// Return the distance between A and B
						if (distance != f2d()) {
							// Player
							if (dynamic_cast<Player*>(this) != nullptr) {
								// Enemy collision
								if (dynamic_cast<Enemy*>(object) != nullptr) {
									std::cout << "Hit by " << object->name << std::endl;
									dynamic_cast<Player*>(this)->Hit(dynamic_cast<Enemy*>(object), allEntities);
								}
								// Exit collision
								if (dynamic_cast<Environment*>(object) != nullptr && dynamic_cast<Environment*>(object)->isExit) {
									std::cout << "Exit" << std::endl;
									// Next level
									if (dynamic_cast<Player*>(this)->taken) {
										dynamic_cast<Player*>(this)->taken = false;
										Level::isFinished = true;
									}
									// Display message else
									else {
										// Display message
										dynamic_cast<Environment*>(object)->message = 0.5;
									}
								}
								// Item collision	
								if (dynamic_cast<Item*>(object) != nullptr) {
									Item* i = dynamic_cast<Item*>(object);
									switch (i->type) {
										// Change armor
										case SUIT:
											std::cout << "Took a suit" << std::endl;
											dynamic_cast<Player*>(this)->ChangeArmor(i);
											i->isActive = false;
											return f2d();
										// Move box
										case BOX:
											std::cout << "Hit a box" << std::endl;
											if (dynamic_cast<Player*>(this)->armor && dynamic_cast<Player*>(this)->armor->name == "HeavySuit") {
												i->Move(new f2d(dir->x*2, dir->y), allEntities);
											} else {
												i->Move(dir, allEntities);
											}
											return f2d((dir->x*i->speed->x), 0.1);
										// Take energyball
										case ENERGYBALL:
											std::cout << "Took the EnergyBall" << std::endl;
											dynamic_cast<Player*>(this)->BallTaken(i);
											return f2d();
										default:
											break;
									}
								}
							}

							return distance;
						}
					}
				}
			}
		}
	}

	return f2d();
}

void GameObject::Display() {
	// Print
	KPrintSprite(sprite, *transform, 1, 1);
}