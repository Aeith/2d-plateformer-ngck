#include "Level.h"
#include "LoadLevelFromImage.h"

// Fields
int Level::index = 0;
std::string Level::path = "";
bool Level::isFinished = false;
f2d* Level::size = new f2d();
f2d* Level::offset = new f2d();

// Methods
void Level::Init() {
	// Set player sprite path
	char p[26] = "datas/levels/level";
	std::strcat(p, &std::to_string(Level::index)[0u]);
	std::strcat(p, ".png");
	std::puts(p);

	path = p;
}

bool Level::Load(Player* player, std::vector<std::vector<GameObject*>>& allEntities) {
	sf::Image image;
	// Load file
	if (image.loadFromFile(path)) {
		// Load level
		if (LoadLevelFromImage::sLoadFromImage(image, player, allEntities)) {
			return true;
		}
	}
	
	return false;
}

void Level::Next() {
	// Clear
	Level::isFinished = false;
	Level::index++;

	// New init
	Level::Init();
}

void Level::Reset(std::vector<std::vector<GameObject*>> allEntities) {
	// Reset all items
	for each (std::vector<GameObject*> list in allEntities) {
		for each (GameObject* object in list) { {}
			Level::Reset(object);
		}
	}

	// Reset offset
	Level::offset->x = 0;
	Level::offset->y = 0;
}

void Level::Reset(GameObject* object) {
	// Reset position
	if (object->transform && object->position) {
		object->isActive = true;
		// Reset base position on level
		*object->transform = *object->position;
		// Clear acceleration and movement
		object->direction->x = 0;
		object->direction->y = 0;

		// Reset player data
		if (dynamic_cast<Player*>(object) != nullptr) {
			Player* p = dynamic_cast<Player*>(object);
			p->health = 100;
			p->armor = nullptr;
			p->taken = false;
			p->sprite = KLoadSprite("player.png");
		}
	}
}

void Level::Clear(std::vector<std::vector<GameObject*>>& allEntities) {
	// Delete all objects*
	for (auto i = allEntities.begin(); i != allEntities.end(); i++) {
		for (auto j = (*i).begin(); j != (*i).end(); j++) {
			delete (*j);
			(*j) = nullptr;
		}
		// Clear vectors
		(*i).erase((*i).begin(), (*i).end());
	}
}