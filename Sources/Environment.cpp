#include "Environment.h"


// Constructors / Destructor
Environment::Environment() : GameObject(), isWall(false), isFloor(false), isRoof(false), isPedestral(false), isExit(false) {}
Environment::Environment(bool a, char* n, int s, float m, State st, f2d* spe, f2d* sz, f2d* pos, f2d* t, f2d* dir, bool w, bool f, bool r, bool p, bool e, float msg) : GameObject(a, n, s,  m, st, spe, sz, pos, t, dir), isWall(w), isFloor(f), isRoof(r), isPedestral(p), isExit(e), message(0.0f) {}
Environment::~Environment() {}

// Methods
void Environment::Display() {
	if (this->isExit) {
		// Print message for timer assigned
		if (message > 0) {
			KPrintSprite(KLoadSprite("msg.png"), transform->x - 15, transform->y - 25, 1, 1);
		}
	}

	GameObject::Display();
}