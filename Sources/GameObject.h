#pragma once
#include "Ngck_moteur/Ngck.h"

#include <iostream>
#include <vector>

// Forward declaration
class Level;
class Player;
class Enemy;
class Item;
class Environment;

// ACCELERATION
// g = 9.81ms-�
#define ACCELERATION	9.81

enum State {
	WAITING,
	JUMPING,
	FALLING,
	PUSHING,
	ERROR
};

class GameObject {
private:
	// Return the distance from the object it will collide ; nullptr is none
	f2d WillCollide(f2d* dir, std::vector<std::vector<GameObject*>> allEntities);
	void GameObject::SetPosition(f2d* dir, std::vector<std::vector<GameObject*>> allEntities);

public:
	// Fields
	bool isActive;
	char* name;
	int sprite;
	float mass;
	State state;
	f2d* speed;
	f2d* size;
	f2d* position;
	f2d* transform;
	f2d* direction;

	// Constructors / Desctructor
	GameObject();
	GameObject(bool a, char* name, int spr, float m, State st, f2d* spe, f2d* siz, f2d* pos, f2d* t, f2d* dir);
	virtual ~GameObject();

	// Methods
	GameObject& operator=(const GameObject& object);
	void addSprite(char* path);
	virtual void ApplyGravity(float deltatime, std::vector<std::vector<GameObject*>> allEntities);
	virtual void Move(f2d* dir, std::vector<std::vector<GameObject*>> allEntities);
	virtual void Display();
};

#include "Level.h"
#include "Player.h"
#include "Item.h"
#include "Enemy.h"
#include "Environment.h"