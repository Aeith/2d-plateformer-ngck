#include "LoadLevelFromImage.h"

bool LoadLevelFromImage::sLoadFromImage(sf::Image& pImage, Player*& playR, std::vector<std::vector<GameObject*>>& allEntities) {
	// Retrieve image size
	auto& image = pImage;
	Level::size = new f2d(image.getSize().x, image.getSize().y);
	
	// Load image
	for (int j = 0; j < Level::size->y; ++j) {
		for (int i = 0; i < Level::size->x; ++i) {
			sf::Color c = image.getPixel(i, j);
			c.a = 255;
			
			// Bind color to element
			bool roof = 0 == c.r && 127 == c.g && 127 == c.b;
			bool floor = 127 == c.r && 0 == c.g && 127 == c.b;
			bool wallLeft = 126 == c.r && 127 == c.g && 0 == c.b;
			bool wallRight = 127 == c.r && 126 == c.g && 0 == c.b;	
			bool env = 127 == c.r && 127 == c.g && 127 == c.b;
			bool pedestrial = 10 == c.r && 10 == c.g && 10 == c.b;
			bool exit = 0 == c.r && 0 == c.g && 0 == c.b;

			bool player = 0 == c.r && 0 == c.g && 255 == c.b;
			bool chaser = 0 == c.r && 255 == c.g && 0 == c.b;
			bool guard = 255 == c.r && 0 == c.g && 0 == c.b;

			bool lightSuit = 100 == c.r && 100 == c.g && 255 == c.b;
			bool mediumSuit = 150 == c.r && 150 == c.g && 255 == c.b;
			bool heavySuit = 200 == c.r && 200 == c.g && 255 == c.b;
			bool woodenBox = 255 == c.r && 100 == c.g && 100 == c.b;
			bool steelBox = 255 == c.r && 150 == c.g && 150 == c.b;
			bool energyBall = 255 == c.r && 255 == c.g && 150 == c.b;
			
			// Suits are over pedestrial
			pedestrial = pedestrial || lightSuit;
			pedestrial = pedestrial || mediumSuit;
			pedestrial = pedestrial || heavySuit;

			// Add game object
			if (roof) {
				// Set Gameobject type, sprite and position
				Environment* object = new Environment();
				object->addSprite("roof.png");
				object->name = "Roof";
				object->size = new f2d(19.9, 19.9);

				// Env fields
				object->isFloor = false;
				object->isWall = false;
				object->isRoof = true;
				object->isPedestral = false;
				object->isExit = false;

				// Set position
				object->position = new f2d((float)i * 20, (float)j * 20);
				object->transform = new f2d((float)i * 20, (float)j * 20);

				// Add to the list
				allEntities[0].push_back(object);
			}
			if (floor) {
				// Set Gameobject type, sprite and position
				Environment* object = new Environment();
				object->addSprite("floor.png");
				object->name = "Floor";
				object->size = new f2d(19.9, 19.9);

				// Env fields
				object->isFloor = true;
				object->isWall = false;
				object->isRoof = false;
				object->isPedestral = false;
				object->isExit = false;

				// Set position
				object->position = new f2d((float)i * 20, (float)j * 20);
				object->transform = new f2d((float)i * 20, (float)j * 20);

				// Add to the list
				allEntities[0].push_back(object);
			}
			if (wallLeft) {
				// Set Gameobject type, sprite and position
				Environment* object = new Environment();
				object->addSprite("wallleft.png");
				object->name = "Wall Left";
				object->size = new f2d(19.9, 19.9);

				// Env fields
				object->isFloor = false;
				object->isWall = true;
				object->isRoof = false;
				object->isPedestral = false;
				object->isExit = false;

				// Set position
				object->position = new f2d((float)i * 20, (float)j * 20);
				object->transform = new f2d((float)i * 20, (float)j * 20);

				// Add to the list
				allEntities[0].push_back(object);
			}
			if (wallRight) {
				// Set Gameobject type, sprite and position
				Environment* object = new Environment();
				object->addSprite("wallright.png");
				object->name = "Wall Right";
				object->size = new f2d(19.9, 19.9);

				// Env fields
				object->isFloor = false;
				object->isWall = true;
				object->isRoof = false;
				object->isPedestral = false;
				object->isExit = false;

				// Set position
				object->position = new f2d((float)i * 20, (float)j * 20);
				object->transform = new f2d((float)i * 20, (float)j * 20);

				// Add to the list
				allEntities[0].push_back(object);
			}
			if (env) {
				// Set Gameobject type, sprite and position
				Environment* object = new Environment();
				object->addSprite("env.png");
				object->name = "Env";
				object->size = new f2d(19.9, 19.9);

				// Env fields
				object->isFloor = true;
				object->isWall = true;
				object->isRoof = true;
				object->isPedestral = false;
				object->isExit = false;

				// Set position
				object->position = new f2d((float)i * 20, (float)j * 20);
				object->transform = new f2d((float)i * 20, (float)j * 20);

				// Add to the list
				allEntities[0].push_back(object);
			}
			if (pedestrial) {
				// Set Gameobject type, sprite and position
				Environment* object = new Environment();
				object->addSprite("pedestrial.png");
				object->name = "Pedestrial";
				object->size = new f2d(15, 5);

				// Env fields
				object->isFloor = false;
				object->isWall = false;
				object->isRoof = false;
				object->isPedestral = true;
				object->isExit = false;

				// Set position
				object->position = new f2d((float)i * 20, (float)j * 20);
				object->transform = new f2d((float)i * 20, (float)j * 20);

				// Add to the list
				allEntities[0].push_back(object);
			}
			if (player) {
				// Set Gameobject type, sprite and position
				playR->addSprite("player.png");
				playR->name = "player";
				playR->mass = 70;
				playR->speed = new f2d(10, 10);
				playR->size = new f2d(20, 40);

				playR->state = WAITING;

				// Player fields
				playR->health = 100;

				// Set position
				playR->position = new f2d((float)i * 20, (float)j * 20);
				playR->transform = new f2d((float)i * 20, (float)j * 20);
			}
			if (chaser) {
				// Set Gameobject type, sprite and position
				Enemy* object = new Enemy();
				object->addSprite("chaser.png");
				object->name = "chaser";
				object->mass = 5;
				object->speed = new f2d(3, 3);
				object->size = new f2d(5, 5);

				// Enenmy fields
				object->sight = 150;
				object->range = 25;
				object->strength = 1;

				// Set position
				object->position = new f2d((float)i * 20, (float)j * 20);
				object->transform = new f2d((float)i * 20, (float)j * 20);

				// Add to the list
				allEntities[1].push_back(object);
			}
			if (guard) {
				// Set Gameobject type, sprite and position
				Enemy* object = new Enemy();
				object->addSprite("guard.png");
				object->name = "guard";
				object->mass = 100;
				object->size = new f2d(40, 40);

				// Enenmy fields
				object->sight = 100;
				object->range = 50;
				object->strength = 200;

				// Set position
				object->position = new f2d((float)i * 20, (float)j * 20);
				object->transform = new f2d((float)i * 20, (float)j * 20);

				// Add to the list
				allEntities[1].push_back(object);
			}
			if (lightSuit) {
				// Set Gameobject type, sprite and position
				Item* object = new Item();
				object->addSprite("lightsuit.png");
				object->name = "LightSuit";
				object->mass = 5;
				object->size = new f2d(15, 5);

				// Item fields
				object->type = SUIT;
				object->defense = 1;

				// Set position
				object->position = new f2d((float)i * 20, (float)j * 20);
				object->transform = new f2d((float)i * 20, (float)j * 20);

				// Add to the list
				allEntities[2].push_back(object);
			}
			if (mediumSuit) {
				// Set Gameobject type, sprite and position
				Item* object = new Item();
				object->addSprite("mediumsuit.png");
				object->name = "MediumSuit";
				object->mass = 15;
				object->size = new f2d(15, 5);

				// Item fields
				object->type = SUIT;
				object->defense = 5;

				// Set position
				object->position = new f2d((float)i * 20, (float)j * 20);
				object->transform = new f2d((float)i * 20, (float)j * 20);

				// Add to the list
				allEntities[2].push_back(object);
			}
			if (heavySuit) {
				// Set Gameobject type, sprite and position
				Item* object = new Item();
				object->addSprite("heavysuit.png");
				object->name = "HeavySuit";
				object->mass = 40;
				object->size = new f2d(15, 5);

				// Item fields
				object->type = SUIT;
				object->defense = 100;

				// Set position
				object->position = new f2d((float)i * 20, (float)j * 20);
				object->transform = new f2d((float)i * 20, (float)j * 20);

				// Add to the list
				allEntities[2].push_back(object);
			}
			if (woodenBox) {
				// Set Gameobject type, sprite and position
				Item* object = new Item();
				object->addSprite("woodbox.png");
				object->name = "Woodbox";
				object->mass = 5;
				object->speed = new f2d(0.5, 0.5);
				object->size = new f2d(19.9, 19.9);

				// Item fields
				object->type = BOX;
				object->defense = NULL;

				// Set position
				object->position = new f2d((float)i * 20, (float)j * 20);
				object->transform = new f2d((float)i * 20, (float)j * 20);

				// Add to the list
				allEntities[2].push_back(object);
			}
			if (steelBox) {
				// Set Gameobject type, sprite and position
				Item* object = new Item();
				object->addSprite("steelbox.png");
				object->name = "Steelbox";
				object->mass = 35;
				object->speed = new f2d(0.15, 0.15);
				object->size = new f2d(19.9, 19.9);

				// Item fields
				object->type = BOX;
				object->defense = NULL;

				// Set position
				object->position = new f2d((float)i * 20, (float)j * 20);
				object->transform = new f2d((float)i * 20, (float)j * 20);

				// Add to the list
				allEntities[2].push_back(object);
			}
			if (energyBall) {
				// Set Gameobject type, sprite and position
				Item* object = new Item();
				object->addSprite("energyball.png");
				object->name = "Energy Ball";
				object->mass = 5;
				object->size = new f2d(20, 20);

				// Item fields
				object->type = ENERGYBALL;
				object->defense = NULL;

				// Set position
				object->position = new f2d((float)i * 20, (float)j * 20);
				object->transform = new f2d((float)i * 20, (float)j * 20);

				// Add to the list
				allEntities[2].push_back(object);
			}
			if (exit) {
				// Set Gameobject type, sprite and position
				Environment* object = new Environment();
				object->addSprite("exit.png");
				object->name = "Exit";
				object->size = new f2d(19.9, 39.9);

				// Env fields
				object->isFloor = false;
				object->isWall = false;
				object->isRoof = false;
				object->isPedestral = false;
				object->isExit = true;

				// Set position
				object->position = new f2d((float)i * 20, (float)j * 20);
				object->transform = new f2d((float)i * 20, (float)j * 20);

				// Add to the list
				allEntities[1].push_back(object);
			}
		}
	}

	return true;
}