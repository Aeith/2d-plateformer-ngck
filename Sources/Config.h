

/// ABORTED
/// CONSUMING TO MUCH RESOURCES
///

#pragma once
#include "GameObject.h"
#include "Item.h"

#include <iostream>
#include <string>
#include <fstream>

// Forward declaration
class GameObject;
class Item;

class Config {
private:
	Config() = delete;
	~Config() = delete;
public:
	static std::string findFromConfig(std::string k, std::string s); // Load data from config file
	static bool stob(std::string s);
	static State stos(std::string s); // Return GameObject State
	static Type stot(std::string s); // Return Item Type
};