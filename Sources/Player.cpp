#include "Player.h"

// Constructors / Desctructors
Player::Player() : GameObject(), taken(false), health(0), armor(nullptr) {}
Player::Player(bool a, char* n, int s, float m, State st, f2d* spe, f2d* sz, f2d* pos, f2d* t, f2d* dir, int h) : GameObject(a, n, s, m, st, spe, sz, pos, t, dir), taken(false), health(h), armor(nullptr) {}
Player::Player(bool a, char* n, int s, float m, State st, f2d* spe, f2d* sz, f2d* pos, f2d* t, f2d* dir, int h, Item* ar) : GameObject(a, n, s, m, st, spe, sz, pos, t, dir), taken(false), health(h), armor(ar) {}
Player::~Player() {
	delete armor;
	armor = nullptr;
}

// Methods
void Player::ChangeArmor(Item* a) {
	// Set player sprite path
	char text[22] = "player";
	std::strcat(text, a->name);
	std::strcat(text, ".png");
	std::puts(text);

	// Change player sprite
	sprite = KLoadSprite(text);
	std::cout << "Changed Player's sprite to " << text << std::endl;

	// Change armor object
	if(armor)
		this->armor->isActive = true;
	this->armor = a;
	std::cout << "Armored changed" << std::endl;
}

void Player::BallTaken(Item* b) {
	taken = true;

	// Place ball in the left hand
	b->transform->x = transform->x + (size->x / 2);
	b->transform->y = transform->y + (size->y / 2);
}

int Player::Hit(Enemy* e, std::vector<std::vector<GameObject*>> allEntities) {
	float damage = 0.0f;
	std::cout << "Got hit by " << e->name << std::endl;

	// Calculate damage based on armor defense
	if (armor) {
		damage = (e->strength / armor->defense);
	}
	else {
		// No armor = twice damages
		damage = (e->strength * 2);
	}

	std::cout << "-" << damage << " hp" << std::endl;

	// Reduce HP
	health -= damage;

	// Reload if < 0
	if (health <= 0) {
		std::cout << "You died" << std::endl;

		// Reset player
		taken = false;
		health = 100;
		armor = nullptr;
		sprite = KLoadSprite("player.png");

		// Reset level
		Level::Reset(allEntities);
		Level::Reset(this);
	}

	return damage;
}

bool Player::CheckOffset(f2d* dir, std::vector<std::vector<GameObject*>> allEntities) {
	// Apply offset if player is on the screen's border
	// Level wider than screen width
	if (Level::size->x * 20 > SCREENSIZEX) {
		// 1/4th of the screen width & going left
		if (transform->x < (SCREENSIZEX / 4) && dir->x < 0) {
			return ApplyOffset(dir, allEntities);
		}
		// 3/4th of the screen height & going right
		if ((SCREENSIZEX - transform->x) < (SCREENSIZEX / 4) && dir->x > 0) {
			return ApplyOffset(dir, allEntities);
		}
	}
	// Level taller than screen height
	if (Level::size->y * 20 > SCREENSIZEY) {
		// 1/5th of the screen height & going down
		if (transform->y < (SCREENSIZEY / 4) && dir->y < 0) {
			return ApplyOffset(dir, allEntities);
		}
		// 3/4th of the screen height & going up
		if ((SCREENSIZEY - transform->y) < (SCREENSIZEY / 4) && dir->y > 0) {
			return ApplyOffset(dir, allEntities);
		}
	}

	return true;
}

bool Player::ApplyOffset(f2d* dir, std::vector<std::vector<GameObject*>> allEntities) {
	// X
	if (dir->x != 0) {
		if (dir->x > 0) {
			// Stop at right border
			if (((int) Level::offset->x * 20) + SCREENSIZEX < (Level::size->x * 20)) {
				Level::offset->x += (dir->x / 20);
				return TransformOffset(dir, allEntities);
			}
		}
		else {
			// Stop at left border
			if (Level::offset->x > 0) {
				Level::offset->x += (dir->x / 20);
				return TransformOffset(dir, allEntities);
			}
		}
	}
	// Y
	if (dir->y != 0) {
		if (dir->y > 0) {
			// Stop at bottom border
			if ((int)(Level::offset->y * 20) + SCREENSIZEY < (Level::size->y * 20)) {
				Level::offset->y += (dir->y / 20);
				return TransformOffset(dir, allEntities);
			}
		}
		else {
			// Stop at top border
			if (Level::offset->y > 0) {
				Level::offset->y += (dir->y / 20);
				return TransformOffset(dir, allEntities);
			}
		}
	}

	return true;
}

bool Player::TransformOffset(f2d* dir, std::vector<std::vector<GameObject*>> allEntities) {
	// Apply transform offset on every game object
	for each (std::vector<GameObject*> list in allEntities) {
		for each (GameObject* object in list) {
			if (!(taken && dynamic_cast<Item*>(object) != nullptr && dynamic_cast<Item*>(object)->type == ENERGYBALL)) {
				object->transform->x -= dir->x;
				object->transform->y -= dir->y;
			}
		}
	}

	return false;
}

void Player::ApplyGravity(float deltatime, std::vector<std::vector<GameObject*>> allEntities) {
	// Set offset down
	CheckOffset(direction, allEntities);

	// Apply Gravity then
	GameObject::ApplyGravity(deltatime, allEntities);
}

void Player::Move(f2d* dir, std::vector<std::vector<GameObject*>> allEntities) {
	// Move based on total mass (move slower based on % over base mass)
	float additionnalMass = 0.0f;
	if (armor)
		additionnalMass += armor->mass;
	if (taken) {
		// Retrieve ball mass
		for each (std::vector<GameObject*> list in allEntities) {
			for each (GameObject* object in list) {
				if (dynamic_cast<Item*>(object) != nullptr && dynamic_cast<Item*>(object)->type == ENERGYBALL) {
					additionnalMass += object->mass;
				}
			}
		}
	}

	// Percentage of additionnal mass over base reduced
	float prorata = (additionnalMass / mass);

	// Divide move speed based on add mass
	dir->x -= (dir->x * prorata);
	dir->y -= (dir->y * prorata);

	// True when no offset changes
	// Set offset depending on the direction and position of the player on the screen
	if (CheckOffset(dir, allEntities)) {
		// Move
		GameObject::Move(dir, allEntities);
	}
}

void Player::Display() {
	KPrintSprite(sprite, *transform, 1, 1);
	
	// RED BACKGROUND
	KSetDisplayColor(RED);
	KPrintSquare(transform->x, transform->y - 10, size->x, 5);

	// GREEN HP
	KSetDisplayColor(GREEN);
	KPrintSquare(transform->x, transform->y - 10, ((int) size->x * health) / 100, 5);
	KSetDisplayColor(WHITE);
}