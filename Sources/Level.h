#pragma once
#include "Ngck_moteur/Ngck.h"
#include "GameObject.h"
#include "Player.h"
#include "Item.h"
#include "Enemy.h"
#include "Environment.h"

#include <string>

class Level {
private:
public:
	// Fields
	static int index;
	static std::string path;
	static bool isFinished;
	static f2d* size;
	static f2d* offset;

	// Constructors / Desctructor
	Level() = delete;
	~Level() = delete;

	// Methods
	static void Init();
	static bool Load(Player* player, std::vector<std::vector<GameObject*>>& allEntities);
	static void Next();
	static void Reset(std::vector<std::vector<GameObject*>> allEntities);
	static void Reset(GameObject* object);
	static void Clear(std::vector<std::vector<GameObject*>>& allEntities);
};