#include "Enemy.h"

// Constructors / Desctructor
Enemy::Enemy() : GameObject(), isChasing(false), sight(0), strength(0.0f), range(0) {}
Enemy::Enemy(bool a, char* n, int s, float m, State st, f2d* spe, f2d* sz, f2d* pos, f2d* t, f2d* dir, int si, float str, int r) : GameObject(a, n, s, m, st, spe, sz, pos, t, dir), isChasing(false), sight(si), strength(str), range(r) {}
Enemy::~Enemy() {}

// Methods
bool Enemy::inSight(Player* p) {
	float distance = p->transform->dist(*transform);
	if (abs(distance) <= sight) {
		return true;
	}
	return false;
}

bool Enemy::inRange(Player* p) {
	float distance = p->transform->dist(*transform);
	if (abs(distance) <= range) {
		return true;
	}
	return false;
}

void Enemy::checkDistance(Player* p, std::vector<std::vector<GameObject*>> allEntities) {
	// Check if is in sight -> turn on enemy
	// Set player sprite path
	char text[20] = "";
	std::strcat(text, name);
	if (inSight(p)) {
		std::cout << "Seen by " << name << std::endl;
		std::strcat(text, "On.png");
		std::puts(text);

		// Chaser now follow player
		if (name == "chaser") {
			isChasing = true;
		}
	}
	else {
		std::strcat(text, "Off.png");
		std::puts(text);

		// Stop chasing player
		isChasing = false;
	}
	sprite = KLoadSprite(text);

	// Chase player
	if (isChasing) {
		std::cout << name << " is chasing you" << std::endl;

		// Follow player
		Chase(p);
	}

	// Check if is in range -> attack
	if (inRange(p)) {
		std::cout << "In attack range of " << name << std::endl;

		// Attack player
		p->Hit(this, allEntities);
	}
}

void Enemy::Chase(Player* p) {
	f2d normalized = *p->transform - *transform;
	normalized.normalize();

	// Move
	*transform += (normalized * (*speed));
}