///
/// ABORTED
/// CONSUMING TO MUCH RESOURCES
///

#include "Config.h"

// Return value in the config file from a key specified
// key std::string Name of the value 
// sector std::string Name of the sector []
// When the sector is null ; search thourgh the entire file
std::string Config::findFromConfig(std::string k, std::string s) {
	std::cout << "Reading config file..." << std::endl;
	if (!s.empty()) {
		std::cout << "Sector " << s << std::endl;
	}
	std::cout << "Searching " << k << std::endl;

	// Input stream
	std::ifstream input("Sources/config.ini");
	std::string result;
	bool isSector = false;

	// Loop through all the file
	while (input) {
		std::string sector;
		std::string line;
		std::string key;
		std::string value;

		// Pass the line if commented or empty
		std::getline(input, line);
		if (line[0] == '#' || line.empty())
			continue;

		// Retrieve values if sector is set and correct or no sector set
		if (s.empty() || (!s.empty() && (s == line || isSector))) {
			isSector = true;
			// Read up to the delimiter into key
			// Read up to the newline into value
			std::getline(input, key, '=');
			std::getline(input, value, '\n');

			// Get value if key is correct
			if (key == k) {
				// Return value if not null
				if (value == "NULL") {
					std::cout << "Found NULL value" << std::endl;
					return "0";
				}
				std::cout << "Value found : " << value << std::endl;
				return value;
			}
		}
	}

	// Close file
	input.close();
	std::cout << "No value found" << std::endl;
	return "0";
}

bool Config::stob(std::string s) {
	if (s != "" && s == "true") {
		return true;
	} else {
		return false;
	}
}

// Return GameObject State
State Config::stos(std::string s) {
	if (s == "NULL") {
		return ERROR;
	}

	if (s == "WAITING") {
		return WAITING;
	} else if (s == "JUMPING") {
		return JUMPING;
	} else if (s == "FALLING") {
		return FALLING;
	} else if (s == "PUSHING") {
		return PUSHING;
	} else {
		return ERROR;
	}
}

// Return Item Type
Type Config::stot(std::string s) {
	if (s == "NULL") {
		return ERR;
	}

	if (s == "SUIT") {
		return SUIT;
	} else if (s == "BOX") {
		return BOX;
	} else if (s == "ENERGYBALL") {
		return ENERGYBALL;
	} else {
		return ERR;
	}
}