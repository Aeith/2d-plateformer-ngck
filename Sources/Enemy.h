#pragma once
#include "Ngck_moteur/Ngck.h"
#include "GameObject.h"
#include "Player.h"
#include "Item.h"
#include "Enemy.h"
#include "Environment.h"

class Enemy : public GameObject {
private:
	bool isChasing;

	// Methods
	bool inSight(Player* p);
	bool inRange(Player* p);
	void Chase(Player* p);

public:
	// Fields
	int sight;
	int range;
	float strength;

	// Constructors / Desctructor
	Enemy();
	Enemy(bool a, char* n, int s, float m, State st, f2d* spe, f2d* sz, f2d* pos, f2d* t, f2d* dir, int si, float str, int r);
	virtual ~Enemy();

	// Methods
	void checkDistance(Player* p, std::vector<std::vector<GameObject*>> allEntities);
};