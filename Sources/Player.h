#pragma once
#include "Ngck_moteur/Ngck.h"
#include "GameObject.h"
#include "Player.h"
#include "Item.h"
#include "Enemy.h"

#include <string.h>

class Player : public GameObject {
private:
	// Methods
	bool CheckOffset(f2d* dir, std::vector<std::vector<GameObject*>> allEntities);
	bool ApplyOffset(f2d* dir, std::vector<std::vector<GameObject*>> allEntities);
	bool TransformOffset(f2d* dir, std::vector<std::vector<GameObject*>> allEntities);
	
public:	
	// Fields
	bool taken;
	int health;
	Item* armor;

	// Constructors / Desctructor
	Player();
	Player(bool a, char* n, int s, float m, State st, f2d* spe, f2d* sz, f2d* pos, f2d* t, f2d* dir, int h);
	Player(bool a, char* n, int s, float m, State st, f2d* spe, f2d* sz, f2d* pos, f2d* t, f2d* dir, int h, Item* ar);
	virtual ~Player();

	// Methods
	void ChangeArmor(Item* a);
	void BallTaken(Item* b);
	int Hit(Enemy* e, std::vector<std::vector<GameObject*>> allEntities);
	void ApplyGravity(float deltatime, std::vector<std::vector<GameObject*>> allEntities) override;
	void Move(f2d* dir, std::vector<std::vector<GameObject*>> allEntities) override;
	void Display() override;
};