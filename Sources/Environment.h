#pragma once
#include "Ngck_moteur/Ngck.h"
#include "GameObject.h"

class Environment : public GameObject {
private:

public:
	// Fields
	bool isWall;
	bool isFloor;
	bool isRoof;
	bool isPedestral;
	bool isExit;
	float message;

	// Constructors / Destructor
	Environment();
	Environment(bool a, char* n, int s, float m, State st, f2d* spe, f2d* sz, f2d* pos, f2d* t, f2d* dir, bool w, bool f, bool r, bool p, bool e, float msg);
	virtual ~Environment();

	// Methods
	void Display() override;
};