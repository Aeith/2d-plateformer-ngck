#pragma once
#include "Ngck_moteur/Ngck.h"
#include "GameObject.h"
#include "Player.h"

enum Type {
	SUIT,
	BOX,
	ENERGYBALL,
	ERR
};

class Item : public GameObject {
private:

public:
	// Fields
	float defense;
	Type type;

	// Constructors / Desctructor
	Item();
	Item(bool a, char* n, int s, float m, State st, f2d* spe, f2d* sz, f2d* pos, f2d* t, f2d* dir);
	Item(bool a, char* n, int s, float m, State st, f2d* spe, f2d* sz, f2d* pos, f2d* t,  f2d* dir, float d, Type ty);
	virtual ~Item();

	// Methods
	void Move(f2d* dir, std::vector<std::vector<GameObject*>> allEntities) override;
};